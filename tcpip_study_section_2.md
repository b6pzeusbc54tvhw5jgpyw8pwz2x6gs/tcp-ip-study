# Chapter 2. 네트워크 서비스와 애플리케이션 계층
> TCP/IP 쉽게 더 쉽게, 30p
- 웹 서비스화라는 트렌드: 새로운 프로토콜을 만드는 것 보다 기존 HTTP 를 활용.

## 01. 애플리케이션 계층의 역할
- 직접 사용하는 프로토콜: HTTP, POP, SMTP, IMAP, SMB, AFP, FTP, Telnet, SSH
- 간접 사용하는 프로토콜: DNS, DHCP, SSL/TLS, NTP, LDAP

## 02. 웹 페이지를 전송하는 HTTP

### 02.1. 웹 페이지가 표시되기까지의 과정
HTTP/1.1
```
         client                          server
   웹 페이지 URL 접근            
                                        HTML 응답
   응답받은 HTML 을 browser 처리,
   JS, CSS, image 파일 등이 필요하면
   해당 파일들을 받기위해
   파일의 갯수만큼 새로운 요청을 보냄
                                        각 요청에 대한 파일들을 응답
   응답받은 파일들을 조합하여
   최종 페이지를 렌더
```

> HTTP/2.0 ??

### 02.2. HTTP 메시지
```http
HTTP/1.1 200 OK
Date: Sun, 10 Oct 2010 23:26:07 GMT
Server: Apache/2.2.8 (Ubuntu) mod_ssl/2.2.8 OpenSSL/0.9.8g
Last-Modified: Sun, 26 Sep 2010 22:04:35 GMT
ETag: "45b6-834-49130cc1182c0"
Accept-Ranges: bytes
Content-Length: 13
Connection: close
Content-Type: text/html

Hello world!
```

https://en.wikipedia.org/wiki/List_of_HTTP_header_fields


example1) write server.js
```
$ curl -D- 127.0.0.1:9000
```

example2) write server2.js
example3) write server3.js
example4) express

HTTP 는 (상태비저장) stateless 방식
> stateless 라는 용어는 여러 분야에서 많이 사용됨.

example5) 200 / 304 test `-H If-Modified-Since: ...`

request header 를 보고 싶으면, -v

example6) write server4.js
> chrome browser 에서 from-disk 사용. (서버에 요청조차하지않음)
> `chrome://cache` 에서 확인할수 있는 캐싱된 데이터들을 사용하는 듯.

### 02.3. HTTP 요청과 URL
- 책에 있는 정보는 중딩용. [wiki: URI][uri] 가 정확함.
- [encodeURIComponent][encode_uri_component]
- [qs][qs], [url][url]

> ALB, vHost 설명

### 02.4. HTTP 응답과 상태 코드
- [wiki: http status][http_status]
- API 설계시 예:
  - 기모웹) 무조건 200 status 를 주소 error 코드를 정의하여 내려주는 방법.
  - 어터툴) 의미에 맞는 status 를 셋팅하여 내려주는 방법.

## 03. 웹 서비스와 웹 애플리케이션
- 책에서의 웹 서비스는, 쉽게 말해 API 서버. `웹 서비스` 라고 하면 웹브라우저에서 실행되는 서비스 같은느낌이라 오해의 소지가 있음.
- 과거 CGI 라는 것도 있었다. 심심하면 찾아보기.

### 03.3. GET 방식과 POST 방식
- [wiki: http][http]
- [spec: http][http_spec]

오홍 그렇다면,

- Q. GET 과 POST 중 뭐가 더 안전할까?
  - [wiki: http safe method][safe_method]

- Q. 멱등성이란?
  - 여러번 연산하더라도 결과가 달라지지 않는 성질
  - 0은 덧셈의 멱등원, 1은 곱셉의 멱등원
  - [wiki: 멱등성][멱등성]
  - [wiki: http idemptent method][http_idempotent]

- Q. RESTFul API 란?
  - [blog: RESTFul][restful]
  - CRUD - Create Read Update Delete
  - Create : 생성(POST)
  - Read : 조회(GET)
  - Update : 수정(PUT)
  - Delete : 삭제(DELETE)
  - 그럼 좋은거니?
    - 꼭 그렇지많은 않음.
    - 헷갈림. 애매한 경우 client 개발자가 혼란.
      - 예) 카드등록일 경우 새카드 등록은 POST 이고 카드가 있었으면 PUT 인가?
    - 개인적으로 무조건 post 를 쓰는 편 - 속편함.


### 03.4. 웹 서비스의 사용성을 높여주는 AJAX
과거 Ajax 가 없었을땐, 무족건 `<form>` 을 사용했어야함. 그래서 회원가입시 form 을 채워서 submit 하면 무조건 응답을 노출해줬어야함. 예) **가입을축하합니다** 페이지.

- Q. AJAX 가 어떤 용어의 약어인지?

- Q. 그럼 JSON 은?

- Q. json 을 더 많이 사용하는데 왜 이름에 XML이 들어갔을까? 
  - MS 의 정치(정책)적인 이유 [wiki: ajax (programming)][Ajax(programming)]

## 04. 세션을 유지하기 위한 쿠키

- [cookie from hell][cookie_from_hell]
- [wiki: fortune cookie][fortune_cookie]

Where Chrome save cookie
```
# MAC OS
$ file ~/Library/Application Support/Google/Chrome/Default/Cookies
Cookies: SQLite 3.x database
```

- 쿠키를 굽워라~ 'Set-Cookie' 헤더로 응답
- 보안 관련 조치
  - plain 텍스트가 아닌 sqllite3 database 로 저장해서, 클릭해서 볼수 있는 정도는 아니지만, 간단한 코드를 짜서 확인할 수 있음.
  - 최신버전 브라우저 - value 를 암호화하여 저장
  - Chrome Safe Storage 키체인 사용한다는 컨펌창에서 allow 하면 바로 보임.
  - 이런 컨펌창의 의미는 의도치않게 실행되는 코드에서 정보를 빼가는 것을 막음.
  - ex) go run cookiemonster.go

- 아무튼 보이긴 보이므로, 민감정보 저장 주의! `SESSION_ID` 정도만 저장
- 기모웹 아키텍쳐 사례.
  - setMain 시 GM에서 전달한 정보를 쿠키로 구워줌
  - 이후 쿠키를 계속 전달하며 통신
  - 서버에서는 요청을 받을때, deviceModel 등을 찾기위해 세션을 뒤지고, 캐쉬를 뒤지고, 쿠키를 뒤지고......?
  - 왜?? 쿠키를 계속 전달하고 있었으면 그냥 쿠키만 뒤지면 되잖아.

  - 세션만료시 API 요청 실패로 접속후 1시간이 지나면 웹스토어를 껐다켜야했음.
  - 몇년 후 이렇게 고침.
    - 세션 날아가면 특정 에러코드로 응답.
    - client 에서 특정 에러코드가 오면 모든 캐쉬 응답을 keep 해두고 캐쉬복구 api 요청
    - server 에서 쿠키를 보고 세션을 복구시킴.
    - client 에서 login 중이었는지를 판단 한 후, login 정보 복구 위해 client 에서 GM 에 login 요청 및 확인 polling. (로긴정보는 쿠키로 복구 불가능하므로)
    - 로긴이 완료되면 keep 해뒀던 API 다시 요청.

  - 왜 이렇게 하지? 많은 서비스들이 한번 로긴하면 그 이후에 다시 안하잖아!
  - 상태비저장 프로토콜인 HTTP 로 persistent login 구현 사례 (페북관려자?)
  - http://cusmaker.tistory.com/209
  - http://jaspan.com/improved_persistent_login_cookie_best_practice

  - 사용성과 보안의 밸런스를 고려한 정책적 의사결정이 필요.
    - 예: 공격자의 요청일지도 모르는 상황에서도 list 는 그냥 허락해줌.
    - but 유료앱 구매시 password 확인.

## 05. 이메일
HTTP 와는 달리 stateful 프로토콜
- POP3 - 옛날, 넷츠고나 하나로 통신때 많이 사용
- IMAP - 지금의 포털이나 single mail 등
- SMTP
  - send mail, node mailer
  - 사내 - smtpsys
  - Gmail
  - AWS

## 06. PC끼리 파일 공유하기
- NAS
- SMB, AFP: 랜에 연결되면 지들끼리 인사함
- https://aws.amazon.com/ko/efs/

## 07. 파일을 전송하는 FTP
- cafe24 같은 아재 중소 호스팅 업체에서는 아직도 많이 쓸듯?
- 액티브 모드와 패시브 모드: ingress egress 설정에 따라

## 08. 원격지의 컴퓨터 제어하기
- Telnet: 아재. 사용할 줄 모르나 보안이슈 체크시 이걸로 털리는 경우가 있음. <- 이정도는 아는 것도 좋을듯.
- SSH: 유닉스 계열
- RDP: 윈도우에 내장됨
- RFB - VNC 에서 사용

## 09. Voice over IP와 영상 스트리밍
- 정확도보다는 속도를 중시하는 UDP 사용.
- 요즘은 동영상 공유도 http 로 하는 경우가 많음. HTML5 `<video>`

## 10. 크롬 개발 도구로 HTTP 메시지 살펴보기
- 걍 아무 사이트나 들어가서 보면됨

[ajax(programming)]: https://en.wikipedia.org/wiki/Ajax_(programming)#History
[http]: https://ko.wikipedia.org/wiki/HTTP
[safe_method]: https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Safe_methods
[http_spec]:bhttps://tools.ietf.org/html/rfc2616#section-5.1.1
[멱등성]: https://ko.wikipedia.org/wiki/%EB%A9%B1%EB%93%B1%EB%B2%95%EC%B9%99
[http_idempotent]: https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Idempotent_methods_and_web_applications
[restful]: http://sonim1.tistory.com/105
[uri]: https://en.wikipedia.org/wiki/Uniform_Resource_Identifier#Syntax
[http_status]: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
[fortune_cookie]: https://en.wikipedia.org/wiki/Fortune_cookie
[cookie_from_hell]: https://gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/tcp-ip-study/raw/master/cookie.jpg
[encode_uri_component]: https://en.wikipedia.org/wiki/Percent-encoding
[url]: https://www.npmjs.com/package/url
[qs]: https://www.npmjs.com/package/qs