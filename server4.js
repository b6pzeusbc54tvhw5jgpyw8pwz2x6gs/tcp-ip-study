var http = require('http')
var fs = require('fs')
var path = require('path')

function handler( req, res ) {
  console.log('Received request for path: ' + req.url )
  var fileName = 'index.html'
  var filePath = path.resolve( __dirname, fileName )
  var stat = fs.statSync( filePath )

  res.writeHead( 200, {
    'Age': 7,
    'Cache-Control': 'max-age=10',
  })
  fs.createReadStream( filePath ).pipe( res )
}

var server = http.createServer( handler )
server.listen( 9000 )
