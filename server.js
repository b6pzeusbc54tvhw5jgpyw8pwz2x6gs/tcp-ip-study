var http = require('http')

function handler( req, res ) {
  console.log('Received request for path: ' + req.url )
  // res.setHeader( 'Date', 'hi' )
  res.writeHead(200)
  res.end('hello')
}

var server = http.createServer( handler )
server.listen( 9000 )
